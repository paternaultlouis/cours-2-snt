Ce dépôt contient mes supports de cours, devoirs, etc. pour une classe de seconde générale et technologique(dans l'enseignement public français) pour les [sciences numériques et technologie](https://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=138143). Il ne contient que les sources (principalement en LaTeX et Markdown). Ce dépôt est aussi accessible sous forme de [de site web](http://snt.ababsurdo.fr).

Ces documents sont publiés sous license [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), qui en permet, entre autre, la modification et la réutilisation en classe, gratuitement ([j'explique ici](http://ababsurdo.fr/blog/pourquoi_publier_sous_licence_libre/) mon choix de diffusion libre).

Pour compiler le site web, il installer [lektor](http://getlektor.com), puis lancer ``lektor build``.

Pour me contacter, voir [la page dédiée](http://ababsurdo.fr/apropos/).
