title: Tâche finale
---
sort_key: 8
---
image: H96566k.jpg
---
body:

L'objet de cette tâche finale est de créer une page web, à propos d'une grande figure de l'informatique.

{% from 'macros/bootstrap.html' import alertstart, alertend with context %}

{{ alertstart(level="warning", title="")}}
Attention ! Ce travail sera noté, mais pour l'évaluation, la <em>forme</em> aura beaucoup plus d'importance que le <em>fond</em>. Pas la peine d'aller chercher beaucoup d'informations sur votre thème : une ou deux phrases par paragraphe suffit.
{{ alertend() }}

## Préliminaire

- Choisissez une femme importante dans l'histoire de l'informatique. Vous pouvez chercher :

  - dans cette [chronologie des femmes en informatique](https://fr.wikipedia.org/wiki/Chronologie_des_femmes_en_informatique) ;
  - dans cette liste [d'informaticienne par nationalité](https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Informaticienne_par_nationalit%C3%A9) ;
  - ou n'importe qui d'autre, avec l'accord du professeur.

- Lisez la [grille d'évaluation](evaluation-tache-finale.pdf) pour voir comment vous serez notés.

- Ouvrez ce [mémo](memo-html.pdf)[^ganaelrenault], dans lequel vous trouverez les principales balises pour mettre en forme votre page.

[^ganaelrenault]: Mémo réalisé par Ganaël Renault, publié sous licence *Creative Commons by-nc-sa*. Merci à lui.

## Mise en place de l'environnement de travail

0. Dans votre répertoire personnel, créez un dossier pour accueillir les fichiers de ce travail (par exemple `SNT\web\tâche-finale` ou `SNT-web-tachefinale`, ou n'importe quoi que vous pourrez retrouver.
0. Téléchargez (clic droit sur le lien, puis `Enregistrer la cible du lien sous…`) les fichiers [`page.html`](page.html) et [`style.css`](style.css) dans ce dossier.
0. Ouvrez les deux fichiers `page.html` et `style.css` avec le logiciel `Notepad++`.

### Au travail !

Modifiez le fichier `page.html` pour compléter les informations sur le personnage que vous avez choisi.

Reportez vous à la [grille d'évaluation](evaluation-tache-finale.pdf) pour voir ce qui doit obligatoirement figurer sur votre page web.

## Fin

À la fin de la séance, [créez une archive](/tutoriels/creer-une-archive/) avec tous les document de votre site web (au moins un fichier *html* et un fichier *css*, prablement une image *jpg* ou *png*, et peut-être d'autres fichiers encore), et déposez-la sur Pronote.
