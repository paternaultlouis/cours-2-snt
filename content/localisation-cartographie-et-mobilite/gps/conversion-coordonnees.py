import math


def degres_vers_degresminutes(degres):
    """Conversion des degrés en degrés-minutes"""
    return (
        math.trunc(degres),
        (degres - math.trunc(degres)) * 60,
    )


def degres_vers_degresminutessecondes(degres):
    """Conversion des degrés en degrés-minutes-secondes"""
    entier = math.trunc(degres)
    reste = degres - entier

    minutes = math.trunc(reste * 60)
    reste = reste - minutes / 60

    secondes = 60 * 60 * reste

    return (
        entier,
        minutes,
        secondes,
    )


def degresminutes_vers_degres(degres, minutes):
    """Conversion des degrés-minutes en degrés"""
    return degres + minutes / 60


def degresminutessecondes_vers_degres(degres, minutes, secondes):
    """Conversion des degrés-minutes-secondes en degrés"""
    return ...


def degresminutes_vers_degresminutessecondes(degres, minutes):
    """Conversion des degrés-minutes en degrés-minutes-secondes"""
    return ...


def degresminutessecondes_vers_degresminutes(degres, minutes, secondes):
    """Conversion des degrés-minutes-secondes en degrés-minutes"""
    return degres_vers_degresminutes(degresminutessecondes_vers_degres(degres, minutes, secondes))


print(degresminutes_vers_degres(45, 31.901))
