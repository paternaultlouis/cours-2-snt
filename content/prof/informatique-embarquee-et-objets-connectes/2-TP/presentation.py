# Copyright 2019-2024 Louis Paternault
#
# This file is part of Jouets.
#
# Jouets is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Jouets is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Jouets.  If not, see <http://www.gnu.org/licenses/>.

import random

from microbit import *
from utime import ticks_ms

###############################################################################
# Création des images pour afficher les secondes

GAUCHE = [
    Image("""99000:99000:99000:99000:99000"""),
    Image("""09000:09000:09000:09000:09000"""),
    Image("""99000:09000:99000:90000:99000"""),
    Image("""99000:09000:99000:09000:99000"""),
    Image("""99000:99000:99000:09000:09000"""),
    Image("""99000:90000:99000:09000:99000"""),
    Image("""99000:90000:99000:99000:99000"""),
    Image("""99000:09000:09000:09000:09000"""),
    Image("""99000:99000:00000:99000:99000"""),
    Image("""99000:99000:99000:09000:99000"""),
]

DROITE = [chiffre.shift_right(3) for chiffre in GAUCHE]


def affiche_secondes(millisecondes):
    """Affiche le nombre de sondes sur la carte."""
    secondes = int(millisecondes / 1000)
    display.show(DROITE[secondes % 10] + GAUCHE[(secondes % 100) // 10])


###############################################################################

début = ticks_ms()

while True:
    if accelerometer.was_gesture("shake"):
        display.show(Image.SAD)
        sleep(2000)
    elif button_a.is_pressed() and button_b.is_pressed():
        if random.random() < 0.1:
            display.set_pixel(
                random.randint(0, 4), random.randint(0, 4), random.randint(0, 9)
            )
    elif button_a.is_pressed():
        display.scroll(str(temperature()))
    elif button_b.is_pressed():
        lecture = accelerometer.get_x()
        if lecture > 500:
            display.show(Image.ARROW_E)
        elif lecture > 250:
            display.show(Image.ARROW_SE)
        elif lecture > -250:
            display.show(Image.ARROW_S)
        elif lecture > -500:
            display.show(Image.ARROW_SW)
        else:
            display.show(Image.ARROW_W)
    elif pin0.is_touched():
        # Remise à zéro du chronomètre
        début = ticks_ms()
    else:
        affiche_secondes(ticks_ms() - début)
