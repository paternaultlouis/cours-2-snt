poids = float(input("Poids de la lettre (en grammes) ? "))

if poids < 20:
    print(0.97)
elif poids < 100:
    print(1.94)
elif poids < 250:
    print(3.88)
else:
    print(5.82)
