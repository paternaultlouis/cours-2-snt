def fibonacci(n):
    if n == 1:
        return 0
    if n == 2:
        return 1

    a = 0
    b = 1
    for i in range(n-1):
        a, b = b, a + b

    return a

print("Le cinquième terme est", fibonacci(5))
print("Le centième terme est", fibonacci(100))
